# TP Cloud Groupe Rainbow

Tristan Leroy

## Architecture

![Image](./Rainbow%20Twitter.png "Title")

L'architecture idéal pour ce projet est composé de plusieurs composant :

- DynamoDB

        Pouvant stocker 10 GiB de donnée
        Permettant un accès en lecture et en écriture pour 1k de requête par heure

- 3 Lambda

        En architecture x86_64 et supportant 1M de requete par mois chacune

- API Gateway

        Devant supporter 3M de requete par mois

- Cognito

        Devant supporter une base de 1000 utilisateur actif par mois

- Bucket S3

        Stockant les fichier static du site web et permettant un accès a tout les utilisateur

- CloudFront

        Devant supporter 3M de requete par mois


## Pricing

- DynamoDB

        Le coût est de 4.30€ par mois pour une base on-demand avec un dataset de 10 GiB
        Avec 1K de requête en lecture et en écriture par heure

- Lambda

        Le coût d'une lambda est de 1.18€ par mois 
        Pour 128Mb de mémoire en architecture x86_64 pour 1M de requete par mois
        Donc un coût de 1.18 * 3 = 3.54€ par mois

- API Gateway

        Le coût de l'api gateway pour 3M de requte par mois est de 9.98€ sans cache

- Cognito

        Pour 1000 Utilisateur actif par mois cognito coûte 50€

- CloudFront

        Le cloudFront coûte environ 3.60€ pour 3M de requete par mois

Pour un coût total de 71.42€ par mois

